import {Pacman} from "./pacman.js";

class Game {    
    
    constructor() {
        this.pacman = new Pacman(this);
        this.game_over = 0;
        this.pacman_spawned = true;
        this.pacman_moving  = false;

        //Listen for pacman movement keys
        document.addEventListener("keydown", (keypress) => {
            var key = keypress.key;
            
            var movement_keys = {
                "ArrowUp": "up", 
                "ArrowDown": "down", 
                "ArrowLeft": "left",
                "ArrowRight": "right"
            };
            
            if (key in movement_keys) {
                var direction = movement_keys[key];
                try {
                    this.pacman.direction = direction;
                    if (this.pacman_spawned == true){
                        this.pacman_animation_id = setInterval(this.pacman.pacman_animation, 100);
                        this.pacman_spawned      = false;
                        
                        
                        if (this.pacman_moving == false) {
                            this.pacman_moving_id = setInterval(this.pacman.pacman_automated_movement, 150, this.pacman);
                        }
                        this.pacman_moving       = true;
                    }
                } 
                catch(error) {
                    if (error != "TypeError: new_field is null") {
                        var error_alert = document.getElementById("error-alert");
                        error_alert.removeAttribute("style");
                        error_alert.innerText = "Error: Please contact a developer. Message: " + error;
                        console.log(error);
                    } 
                }
            }
        });
        

        this.generate_game_field()
    }

    //Generate the game field
    generate_game_field() {
        var class_field_type = {
            "f": "field", 
            "b": "border-field", 
            "p": "field-big-point",
            "s": "pacman-spawn",
        };
        
        var game_field_structure = [
            ["f", "f", "f", "f", "f", "f", "f", "f", "f", "f", "f", "f", "f", "f", "f", "f", "s"],
            ["f", "b", "f", "b", "f", "b", "f", "b", "b", "b", "f", "b", "f", "b", "b", "b", "f"],
            ["f", "b", "f", "b", "f", "f", "f", "f", "b", "f", "p", "b", "f", "b", "f", "f", "f"],
            ["f", "b", "b", "b", "f", "b", "b", "f", "b", "f", "b", "b", "f", "b", "b", "b", "f"],
            ["f", "b", "b", "p", "f", "f", "f", "f", "b", "f", "b", "b", "f", "f", "p", "b", "f"],
            ["f", "b", "b", "b", "f", "b", "b", "f", "b", "f", "f", "f", "f", "b", "b", "b", "f"],
            ["f", "b", "f", "b", "f", "b", "b", "f", "b", "f", "b", "b", "f", "b", "b", "b", "f"],
            ["f", "f", "f", "f", "f", "b", "f", "f", "f", "f", "b", "b", "f", "f", "f", "f", "f"],
            ["f", "b", "b", "b", "b", "f", "b", "b", "b", "f", "b", "b", "f", "b", "b", "b", "b"],
            ["f", "f", "f", "f", "f", "p", "f", "f", "f", "f", "f", "f", "f", "f", "f", "f", "b"],
        ];
    
        var field_row_class =  "field-row";
        var game_field      = document.getElementById("game-field");
    
        for (var row_num = 0; row_num < game_field_structure.length; row_num++){
            var row =  document.createElement("div");
            var row_id = "row" + row_num;
            
            row.classList.add(field_row_class);
            row.setAttribute("id", row_id);
            
            for (var field_num = 0; field_num < game_field_structure[row_num].length; field_num++){
                var field      = document.createElement("div");
                var field_type = game_field_structure[row_num][field_num];
                var field_id = row_id + "field" + field_num;
                
                field.classList.add(class_field_type[field_type]);
                field.setAttribute("id", field_id);
                
                if (field_type == "f" | field_type == "p"){
                    field.innerText = ".";
                }
    
                else if (field_type == "s") {
                    var pacman = document.createElement("img");
                    pacman.src = "static/img/pacman_opened_mouth.png";
                    pacman.setAttribute("id", "pacman");
    
                    field.appendChild(pacman);
                }
                
                row.appendChild(field);
            }
    
            game_field.appendChild(row);
        }
    }

    generate_field_dict(fields_by_class) {
        var field_dict = {};
        
        for(let field of fields_by_class) {
            var field_class = field.className;
            var field_id    = field.id;
            field_dict[field_id] = field_class;
        }

        return field_dict;
    }
    
    reset_game_field() {
        clearInterval(this.pacman_animation_id);
        this.pacman_spawned = true;

        var pacman = document.getElementById("pacman");
        var spawn  = document.getElementsByClassName("pacman-spawn")[0];
        
        var current_pacman_field_id  = pacman.parentElement.id;
        var current_pacman_field_obj = document.getElementById(current_pacman_field_id);
        current_pacman_field_obj.innerHTML   = ".";
        current_pacman_field_obj.style.color = "yellow";
        
        
        var point_fields_by_class = document.getElementsByClassName("field");
        var point_fields_dict     = this.generate_field_dict(point_fields_by_class);
        
        var big_points_by_class = document.getElementsByClassName("field-big-point");
        var big_points_dict     = this.generate_field_dict(big_points_by_class);
        
        var all_fields     = Object.assign({}, point_fields_dict, big_points_dict);
        
        for (let field in all_fields) {
            var field_obj   = document.getElementById(field);
            var field_class = field_obj.className;
            
            if (field_class == "field") {
                field_obj.style.color = "yellow";
            }
            else if (field_class == "field-big-point") {
                field_obj.style.color = "lightblue";
            }
        }

        spawn.appendChild(pacman);
        this.pacman.spawned = true;
    }
    
    update_score(new_field) {
        
        var fields_by_class      = document.getElementsByClassName("field");
        var bPoints_by_class     = document.getElementsByClassName("field-big-point");   
        var all_fields           = Object.assign({}, fields_by_class, bPoints_by_class);
        var all_fields_len       = Object.keys(all_fields).length;
        var points_available_num = 0;
        
        var current_points  = this.pacman.pacman_score;
        var game_scoreboard = document.getElementById("score");
        var point           = 10;
        var big_point       = 50;
        
        var new_field_class = new_field.className;
        var new_field_color = new_field.style.color;

        if (new_field_color != "black") {
            if (new_field_class == "field") {
                current_points += point;
            } 

            else if (new_field_class == "field-big-point") {
                current_points += big_point;
            }
        }

        this.pacman.pacman_score = current_points;
        game_scoreboard.innerText = this.pacman.pacman_score;

        for (let i = 0; i < all_fields_len; i++) {
            var field_color = all_fields[i].style.color;

            if (field_color != "black") {
                var points_available = true;
                points_available_num += 1;  
            }
            
        }
        
        
        if (points_available_num == 0) {
            this.reset_game_field();
        }

    }

}

const game = new Game();