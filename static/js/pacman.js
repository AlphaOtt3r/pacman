export class Pacman {
    constructor(game) {
        this.pacman_mouth = "opened";
        this.pacman_score = 0;               
        this.game         = game;
        this.mouth        = "opened";
        this.direction;
    }

    //Move pacman
    move(direction) {
        var pacman           = document.getElementById("pacman");
        var current_row_id   = pacman.parentElement.parentElement.id;
        var current_field_id = pacman.parentElement.id.replace(current_row_id, "");

        if (direction == "down" | direction == "up") {
            if (direction == "down") {
                pacman.style.transform = "rotate(90deg)";
                var row_pos = +1;
            } 
            else {
                pacman.style.transform = "rotate(-90deg)";
                var row_pos = -1;
            }
            
            var new_row_num     = parseInt(current_row_id.replace("row", "")) + row_pos;
            var new_field_id    = "row" + new_row_num + current_field_id;
            var new_field       = document.getElementById(new_field_id);
            var new_field_class = new_field.className;
            var old_field       = document.getElementById(current_row_id + current_field_id);

            if (new_field_class != "border-field") {
                old_field.innerText  = ".";
                old_field.style.color = "black";

                new_field.innerText = "";
                new_field.style.color = "black";
                new_field.appendChild(pacman);
                

            }
        }

        else if (direction == "left" | direction == "right") {
            if (direction == "left"){
                pacman.style.transform = "scaleX(-1)";
                var field_pos = -1;
            }
            else{
                pacman.style.transform = "none";
                var field_pos = +1;    
            }

            var new_field_num   = parseInt(current_field_id.replace("field", "")) + field_pos;
            var new_field_id    = current_row_id + "field" + new_field_num;
            var new_field       = document.getElementById(new_field_id);
            var new_field_class = new_field.className;
            var old_field       = document.getElementById(current_row_id + current_field_id);

            if (new_field_class != "border-field") {
                old_field.innerText  = ".";
                old_field.style.color = "black";

                new_field.innerText = "";
                new_field.appendChild(pacman);
                

            }
        }

        this.game.update_score(new_field = new_field);
    }

    pacman_automated_movement(pacman) {
        try {
  
            var pacman_attrs = pacman.get_pacman_attributes();
            pacman.move(pacman_attrs["direction"]);
        }

        catch(error){
            if (error != "TypeError: new_field is null") {
                var error_alert = document.getElementById("error-alert");
                error_alert.removeAttribute("style");
                error_alert.innerText = "Error: Please contact a developer. Message: " + error;
                console.log(error);
            } 
        }
    }

    get_pacman_attributes() {
        var attributes      = this;
        var attributes_dict = {};

        for (let attr in attributes) {
            var attr_val          = attributes[attr];
            attributes_dict[attr] = attr_val;
        }
        

        return attributes_dict;
    }

    pacman_animation() {
        var pacman_closed_mth_src = "static/img/pacman_closed_mouth.png";
        var pacman_opened_mth_src = "static/img/pacman_opened_mouth.png"; 
        var pacman                = document.getElementById("pacman");

        if (this.mouth != "opened") {
            pacman.src = pacman_opened_mth_src;
            this.mouth = "opened";
        }
        else {
            pacman.src = pacman_closed_mth_src;
            this.mouth = "closed";
        }
    

    }
    
}